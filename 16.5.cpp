#include <iostream>
#include <time.h>
using namespace std;

const int STR = 5;
const int COL = 5;

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    buf.tm_mday;

    int index = buf.tm_mday % STR;

    int arr[STR][COL]{};
    int sum = 0;

    for (int i = 0; i < STR; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            arr[i][j] = i + j;
            cout << arr[i][j] << " ";     
        }
        cout << endl << endl;
    }

    for (int j = 0; j < COL; j++)
    {
        sum = sum + arr[index][j];
    }
    cout << endl << " On " << buf.tm_mday << " data summa = " << sum << endl;
    return 0;
}
